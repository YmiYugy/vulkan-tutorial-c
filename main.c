
#include "hellotriangle.h"
#include <stdio.h>

int main() {
    printf("%llu\n", sizeof(HelloTriangleApplication));
    HelloTriangleApplication app;
    run(&app);

    return 0;
}