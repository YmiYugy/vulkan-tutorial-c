cmake_minimum_required(VERSION 3.15)

project(cglm C)

set(CMAKE_C_STANDARD 11)



include_directories(${PROJECT_SOURCE_DIR}/include)
file(GLOB SRC src/*c)
add_library(cglm ${SRC})