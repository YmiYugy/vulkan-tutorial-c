//
// Created by Tristan Kobusch on 12.01.20.
//

#ifndef VULKAN_TUTORIAL_HELLOTRIANGLE_H
#define VULKAN_TUTORIAL_HELLOTRIANGLE_H

#include <vulkan/vulkan.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <stdbool.h>

typedef struct {
    GLFWwindow* window;
    VkInstance instance;
    VkDebugUtilsMessengerEXT debugMessenger;
    VkPhysicalDevice physicalDevice;
    VkDevice device;
    VkQueue graphicsQueue;
    VkQueue presentQueue;
    VkSurfaceKHR surface;
    VkSwapchainKHR swapChain;
    VkImage* swapChainImages;
    uint32_t swapChainImageCount;
    VkFormat swapChainImageFormat;
    VkExtent2D swapChainExtent;
    VkImageView* swapChainImageViews;
    VkFramebuffer* swapChainFramebuffers;
    VkRenderPass renderPass;
    VkDescriptorSetLayout descriptorSetLayout;
    VkDescriptorSet* descriptorSets;
    VkPipelineLayout pipelineLayout;
    VkPipeline graphicsPipeline;
    VkCommandPool commandPool;
    VkBuffer vertexBuffer;
    VkDeviceMemory vertexBufferMemory;
    VkBuffer indexBuffer;
    VkDeviceMemory indexBufferMemory;
    VkBuffer* uniformBuffers;
    VkDeviceMemory* uniformBuffersMemory;
    VkDescriptorPool descriptorPool;
    VkCommandBuffer* commandBuffers;
    VkSemaphore* imageAvailableSemaphore;
    VkSemaphore* renderFinishedSemaphore;
    VkFence* inFlightFences;
    VkFence* imagesInFlight;
    size_t currentFrame;
    bool framebufferResized;

    VkDescriptorPool oldDescriptorPool;
    VkDescriptorSet* oldDescriptorSets;
    VkBuffer* oldUniformBuffers;
    VkDeviceMemory* oldUniformBuffersMemory;
    VkSwapchainKHR oldSwapchain;
    VkFormat oldImageFormat;
    VkRenderPass oldRenderPass;
    VkImageView* oldImageViews;
    uint32_t oldImageCount;
    VkFramebuffer* oldFramebuffers;
    VkImage* oldSwapChainImages;
    VkCommandBuffer* oldCommandBuffers;
    VkFence* oldInFlightFences;
    VkFence* oldImagesInFlight;
}HelloTriangleApplication;

void run(HelloTriangleApplication* app);

#endif //VULKAN_TUTORIAL_HELLOTRIANGLE_H
