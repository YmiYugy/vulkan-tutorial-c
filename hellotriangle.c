//
// Created by Tristan Kobusch on 12.01.20.
//

#include "hellotriangle.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <stdalign.h>
#include <cglm/cglm.h>


#ifndef max
#define max(a, b) ((a) > (b) ? (a) : (b))
#endif

#ifndef min
#define min(a, b) ((a) < (b) ? (a) : (b))
#endif

const int WIDTH = 800;
const int HEIGHT = 600;

const mat4 correction_matrix = {
        {1.0f,  0.0f, 0.0f, 0.0f},
        {0.0f, -1.0f, 0.0f, 0.0f},
        {0.0f,  0.0f, 0.5f, 0.5f},
        {0.0f,  0.0f, 0.0f, 1.0f}
};

typedef struct {
    vec2 pos;
    vec3 color;
} Vertex;

const Vertex vertices[] = {
        {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
        {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
        {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
        {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
};

typedef struct {
    _Alignas(16) mat4 model;
    _Alignas(16) mat4 view;
    _Alignas(16) mat4 proj;
} UniformBufferObject;

const uint16_t indices[] = {
        0, 1, 2, 2, 3, 0
};

const size_t MAX_FRAMES_IN_FLIGHT = 2;

const char *const validationLayers[] = {
        "VK_LAYER_KHRONOS_validation"
};

const char *const deviceExtensions[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

void initWindow(HelloTriangleApplication *app);

void framebufferResizeCallback(GLFWwindow *window, int width, int height);
void framebufferDamageCallback(GLFWwindow* window);

void initVulkan(HelloTriangleApplication *app);

void createInstance(HelloTriangleApplication *app);

bool checkValidationLayerSupport();

int getRequiredExtensions(uint32_t *extensionCount, const char **extensions);

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageServerity,
        VkDebugUtilsMessageTypeFlagBitsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
        void *pUserData);

void setupDebugMessenger(HelloTriangleApplication *app);

VkResult CreateDebugUtilsMessengerEXT(
        VkInstance instance,
        const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
        const VkAllocationCallbacks *pAllocator,
        VkDebugUtilsMessengerEXT *pDebugMessenger);

void DestroyDebugUtilsMessengerExt(
        VkInstance instance,
        VkDebugUtilsMessengerEXT debugMessenger,
        const VkAllocationCallbacks *pAllocator);

void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT *createInfo);

void createSurface(HelloTriangleApplication *app);

void pickPhysicalDevice(HelloTriangleApplication *app);

bool isDeviceSuitable(VkPhysicalDevice device, HelloTriangleApplication *app);

typedef struct {
    uint32_t graphicsFamily;
    uint32_t presentFamily;
} QueueFamilyIndices;

bool QueueFamilyIsComplete(QueueFamilyIndices indices);

QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, HelloTriangleApplication *app);

bool checkDeviceExtensionSupport(VkPhysicalDevice device);

typedef struct {
    VkSurfaceCapabilitiesKHR capabilities;
    uint32_t formatCount;
    VkSurfaceFormatKHR *formats;
    uint32_t presentModeCount;
    VkPresentModeKHR *presentModes;
} SwapChainSupportDetails;

SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, HelloTriangleApplication *app);

void createLogicalDevice(HelloTriangleApplication *app);

VkSurfaceFormatKHR chooseSwapSurfaceFormat(const VkSurfaceFormatKHR *availableFormats, uint32_t formatCount);

VkPresentModeKHR chooseSwapPresentMode(const VkPresentModeKHR *availablePresentModes, uint32_t presentModeCount);

VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR *capabilities, HelloTriangleApplication *app);

void createSwapChain(HelloTriangleApplication *app);

void createImageViews(HelloTriangleApplication *app);

void createRenderPass(HelloTriangleApplication *app);

void createDescriptorSetLayout(HelloTriangleApplication* app);

void createGraphicsPipeline(HelloTriangleApplication *app);

int readFile(const char *filename, size_t *size, char **buffer);

VkShaderModule createShaderModule(const char *code, size_t size, HelloTriangleApplication *app);

void createFramebuffers(HelloTriangleApplication *app);

void createCommandPool(HelloTriangleApplication *app);

void createVertexBuffer(HelloTriangleApplication* app);

void createIndexBuffer(HelloTriangleApplication* app);

void createUniformBuffers(HelloTriangleApplication* app);

void createDescriptorPool(HelloTriangleApplication* app);

void createDescriptorSets(HelloTriangleApplication* app);

uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties, HelloTriangleApplication* app);

void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer* buffer, VkDeviceMemory* bufferMemory, HelloTriangleApplication* app);

void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, HelloTriangleApplication* app);

void createCommandBuffers(HelloTriangleApplication *app);

void createSyncObjects(HelloTriangleApplication *app);

void recreateSwapChain(HelloTriangleApplication *app);

void cleanupSwapChain(HelloTriangleApplication *app);

VkVertexInputBindingDescription getBindingDescription();

void getAttributeDescriptions(VkVertexInputAttributeDescription description[2]);

void mainLoop(HelloTriangleApplication *app);

void drawFrame(HelloTriangleApplication *app);

void updateUniformBuffer(uint32_t currentImage, HelloTriangleApplication* app);

void cleanup(HelloTriangleApplication *app);


void run(HelloTriangleApplication *app) {
    *app = (HelloTriangleApplication) { 0 };
    app->oldImageFormat = VK_FORMAT_MAX_ENUM;
    initWindow(app);
    initVulkan(app);
    mainLoop(app);
    cleanup(app);
}


void initWindow(HelloTriangleApplication *app) {
    app->framebufferResized = false;
    glfwInit();

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

    app->window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", NULL, NULL);
    glfwSetWindowUserPointer(app->window, app);
    //glfwSetFramebufferSizeCallback(app->window, framebufferResizeCallback);
    //glfwSetWindowRefreshCallback(app->window, framebufferDamageCallback);

}

void initVulkan(HelloTriangleApplication *app) {
    createInstance(app);
    setupDebugMessenger(app);
    createSurface(app);
    pickPhysicalDevice(app);
    createLogicalDevice(app);
    createSwapChain(app);
    createImageViews(app);
    createRenderPass(app);
    createDescriptorSetLayout(app);
    createGraphicsPipeline(app);
    createFramebuffers(app);
    createCommandPool(app);
    createVertexBuffer(app);
    createIndexBuffer(app);
    createUniformBuffers(app);
    createDescriptorPool(app);
    createDescriptorSets(app);
    createCommandBuffers(app);
    createSyncObjects(app);
}

void mainLoop(HelloTriangleApplication *app) {
    while (!glfwWindowShouldClose(app->window)) {
        glfwPollEvents();
        drawFrame(app);
    }
    vkDeviceWaitIdle(app->device);
}

void cleanup(HelloTriangleApplication *app) {
    cleanupSwapChain(app);



    for(size_t i = 0; i < app->swapChainImageCount; i++){
        vkDestroyBuffer(app->device, app->uniformBuffers[i], NULL);
        vkFreeMemory(app->device, app->uniformBuffersMemory[i], NULL);
    }
    free(app->uniformBuffers);
    free(app->uniformBuffersMemory);

    free(app->descriptorSets);

    vkDestroyDescriptorPool(app->device, app->descriptorPool, NULL);

    vkDestroyDescriptorSetLayout(app->device, app->descriptorSetLayout, NULL);

    vkDestroyBuffer(app->device, app->indexBuffer, NULL);
    vkFreeMemory(app->device, app->indexBufferMemory, NULL);

    vkDestroyBuffer(app->device, app->vertexBuffer, NULL);
    vkFreeMemory(app->device, app->vertexBufferMemory, NULL);

    for (uint32_t i = 0; i < app->swapChainImageCount; i++) {
        vkDestroyFramebuffer(app->device, app->swapChainFramebuffers[i], NULL);
        vkDestroyImageView(app->device, app->swapChainImageViews[i], NULL);
    }
    free(app->swapChainFramebuffers);
    free(app->swapChainImageViews);
    free(app->swapChainImages);

    vkFreeCommandBuffers(app->device, app->commandPool, app->swapChainImageCount, app->commandBuffers);
    vkDestroySwapchainKHR(app->device, app->swapChain, NULL);

    vkDestroyPipeline(app->device, app->graphicsPipeline, NULL);
    vkDestroyPipelineLayout(app->device, app->pipelineLayout, NULL);
    vkDestroyRenderPass(app->device, app->renderPass, NULL);

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(app->device, app->renderFinishedSemaphore[i], NULL);
        vkDestroySemaphore(app->device, app->imageAvailableSemaphore[i], NULL);
        vkDestroyFence(app->device, app->inFlightFences[i], NULL);
    }
    free(app->inFlightFences);
    free(app->imageAvailableSemaphore);
    free(app->renderFinishedSemaphore);
    vkDestroyCommandPool(app->device, app->commandPool, NULL);

    vkDestroyDevice(app->device, NULL);

    if (enableValidationLayers) {
        DestroyDebugUtilsMessengerExt(app->instance, app->debugMessenger, NULL);
    }

    vkDestroySurfaceKHR(app->instance, app->surface, NULL);
    vkDestroyInstance(app->instance, NULL);

    glfwDestroyWindow(app->window);
    glfwTerminate();
}

void createInstance(HelloTriangleApplication *app) {
    if (enableValidationLayers && !checkValidationLayerSupport()) {
        perror("validation layers requested, but not available!");
        exit(-1);
    }

    VkApplicationInfo appInfo = { 0 };
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = "Hello Triangle";
    appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.pEngineName = "No Engine";
    appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appInfo.apiVersion = VK_API_VERSION_1_1;

    VkInstanceCreateInfo createInfo = { 0 };
    createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createInfo.pApplicationInfo = &appInfo;

    uint32_t extensionCount = 0;
    getRequiredExtensions(&extensionCount, NULL);
    const char **extensions = malloc(sizeof(char *) * extensionCount);
    getRequiredExtensions(&extensionCount, extensions);

    createInfo.enabledExtensionCount = extensionCount;
    createInfo.ppEnabledExtensionNames = extensions;

    VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
    if (enableValidationLayers) {
        populateDebugMessengerCreateInfo(&debugCreateInfo);
        createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT *) &debugCreateInfo;
        createInfo.enabledLayerCount = (uint32_t) (sizeof(validationLayers) / sizeof(validationLayers[0]));
        createInfo.ppEnabledLayerNames = validationLayers;
    } else {
        createInfo.enabledLayerCount = 0;
        createInfo.pNext = NULL;
    }

    if (vkCreateInstance(&createInfo, NULL, &app->instance) != VK_SUCCESS) {
        perror("failed to create instance!");
        exit(-1);
    }

    free(extensions);
}

bool checkValidationLayerSupport() {
    uint32_t layerCount;
    vkEnumerateInstanceLayerProperties(&layerCount, NULL);

    VkLayerProperties *availableLayers = malloc(sizeof(VkLayerProperties) * layerCount);
    vkEnumerateInstanceLayerProperties(&layerCount, availableLayers);

    for (uint32_t i = 0; i < sizeof(validationLayers) / sizeof(validationLayers[0]); i++) {
        bool layerFound = false;

        for (uint32_t j = 0; j < layerCount; j++) {
            if (strcmp(availableLayers[j].layerName, validationLayers[i]) == 0) {
                layerFound = true;
                break;
            }
        }

        if (!layerFound) {
            free(availableLayers);
            return false;
        }
    }

    free(availableLayers);
    return true;
}

int getRequiredExtensions(uint32_t *extensionCount, const char **extensions) {
    if (extensions == NULL && extensionCount == NULL) return -1;

    uint32_t glfwExtensionCount = 0;
    const char **glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

    if (extensions == NULL) {
        if (enableValidationLayers) {
            *extensionCount = glfwExtensionCount + 1;
            return 0;
        } else {
            *extensionCount = glfwExtensionCount;
            return 0;
        }
    } else if (extensionCount == NULL) {
        return -1;
    } else {
        if (enableValidationLayers) {
            if (*extensionCount != glfwExtensionCount + 1) {
                return -1;
            } else {
                memcpy(extensions, glfwExtensions, sizeof(char *) * glfwExtensionCount);
                extensions[glfwExtensionCount] = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
                return 0;
            }
        } else {
            if (*extensionCount != glfwExtensionCount) {
                return -1;
            } else {
                memcpy(extensions, glfwExtensions, sizeof(char *) * glfwExtensionCount);
                return 0;
            }
        }
    }
}

VKAPI_ATTR VkBool32 VKAPI_CALL
debugCallback(__attribute__ ((unused)) VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, __attribute__ ((unused)) VkDebugUtilsMessageTypeFlagBitsEXT messageType,
              const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, __attribute__ ((unused)) void *pUserData) {
    fprintf(stderr, "validation layer: %s\n", pCallbackData->pMessage);
    return VK_FALSE;
}

void setupDebugMessenger(HelloTriangleApplication *app) {
    if (!enableValidationLayers) return;

    VkDebugUtilsMessengerCreateInfoEXT createInfo;
    populateDebugMessengerCreateInfo(&createInfo);

    if (CreateDebugUtilsMessengerEXT(app->instance, &createInfo, NULL, &app->debugMessenger) != VK_SUCCESS) {
        perror("failed to set up debug messenger\n");
        exit(-1);
    }
}

VkResult CreateDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo,
                                      const VkAllocationCallbacks *pAllocator,
                                      VkDebugUtilsMessengerEXT *pDebugMessenger) {
    PFN_vkCreateDebugUtilsMessengerEXT func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance,
                                                                                                         "vkCreateDebugUtilsMessengerEXT");
    if (func != NULL) {
        return func(instance, pCreateInfo, pAllocator, pDebugMessenger);
    } else {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void DestroyDebugUtilsMessengerExt(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
                                   const VkAllocationCallbacks *pAllocator) {
    PFN_vkDestroyDebugUtilsMessengerEXT func = (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(instance,
                                                                                                           "vkDestroyDebugUtilsMessengerEXT");
    if (func != NULL) {
        func(instance, debugMessenger, pAllocator);
    }
}

void populateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT *createInfo) {
    *createInfo = (VkDebugUtilsMessengerCreateInfoEXT) { 0 };
    createInfo->sType =
            VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createInfo->messageSeverity =
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
    createInfo->messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
            | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createInfo->pfnUserCallback = debugCallback;
}

void pickPhysicalDevice(HelloTriangleApplication *app) {
    uint32_t deviceCount = 0;
    vkEnumeratePhysicalDevices(app->instance, &deviceCount, NULL);
    if (deviceCount == 0) {
        perror("failed to find GPUs with Vulkan support!");
        exit(-1);
    }

    app->physicalDevice = VK_NULL_HANDLE;

    VkPhysicalDevice *devices = malloc(sizeof(VkPhysicalDevice) * deviceCount);
    vkEnumeratePhysicalDevices(app->instance, &deviceCount, devices);

    for (uint32_t i = 0; i < deviceCount; i++) {
        if (isDeviceSuitable(devices[i], app)) {
            app->physicalDevice = devices[i];
            break;
        }
    }

    if (app->physicalDevice == VK_NULL_HANDLE) {
        perror("failed to find suitable GPU!");
        exit(-1);
    }

    free(devices);
}

bool isDeviceSuitable(VkPhysicalDevice device, HelloTriangleApplication *app) {
    QueueFamilyIndices indices = findQueueFamilies(device, app);
    bool swapChainAdequate = false;
    bool extensionsSupported = checkDeviceExtensionSupport(device);
    if (extensionsSupported) {
        SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device, app);
        swapChainAdequate = swapChainSupport.formatCount != 0 && swapChainSupport.presentModeCount != 0;
        if (swapChainSupport.presentModeCount != 0) {
            free(swapChainSupport.presentModes);
        }
        if (swapChainSupport.formatCount != 0) {
            free(swapChainSupport.formats);
        }
    }

    return QueueFamilyIsComplete(indices) && extensionsSupported && swapChainAdequate;

}

QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device, HelloTriangleApplication *app) {
    QueueFamilyIndices indices;
    indices.graphicsFamily = UINT32_MAX;
    indices.presentFamily = UINT32_MAX;

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, NULL);
    VkQueueFamilyProperties *queueFamilies = malloc(sizeof(VkQueueFamilyProperties) * queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies);
    for (uint32_t i = 0; i < queueFamilyCount; i++) {
        if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            indices.graphicsFamily = i;
        }
        VkBool32 presentSupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, app->surface, &presentSupport);

        if (presentSupport) {
            indices.presentFamily = i;
        }

        if (QueueFamilyIsComplete(indices)) {
            break;
        }
    }

    free(queueFamilies);

    return indices;
}

bool QueueFamilyIsComplete(QueueFamilyIndices indices) {
    return indices.graphicsFamily != UINT32_MAX && indices.presentFamily != UINT32_MAX;
}


void createLogicalDevice(HelloTriangleApplication *app) {
    QueueFamilyIndices indices = findQueueFamilies(app->physicalDevice, app);

    uint32_t uniqueQueueFamilyCount;
    uint32_t uniqueQueuFamilies[2] = {indices.graphicsFamily, indices.presentFamily};
    if (indices.presentFamily == indices.graphicsFamily) {
        uniqueQueueFamilyCount = 1;
    } else {
        uniqueQueueFamilyCount = 2;
    }

    VkDeviceQueueCreateInfo queueCreateInfo[2];
    float queuePriority = 1.0f;
    for (size_t i = 0; i < uniqueQueueFamilyCount; i++) {
        queueCreateInfo[i] = (VkDeviceQueueCreateInfo){0};
        queueCreateInfo[i].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfo[i].queueFamilyIndex = uniqueQueuFamilies[i];
        queueCreateInfo[i].queueCount = 1;
        queueCreateInfo[i].pNext = NULL;
        queueCreateInfo[i].flags = 0;
        queueCreateInfo[i].pQueuePriorities = &queuePriority;
    }

    VkPhysicalDeviceFeatures deviceFeatures = { 0 };

    VkDeviceCreateInfo createInfo = { 0 };
    createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createInfo.pQueueCreateInfos = queueCreateInfo;
    createInfo.queueCreateInfoCount = uniqueQueueFamilyCount;
    createInfo.pEnabledFeatures = &deviceFeatures;
    createInfo.enabledExtensionCount = sizeof(deviceExtensions) / sizeof(deviceExtensions[0]);
    createInfo.ppEnabledExtensionNames = deviceExtensions;
    if (enableValidationLayers) {
        createInfo.enabledLayerCount = sizeof(validationLayers) / sizeof(validationLayers[0]);
        createInfo.ppEnabledLayerNames = validationLayers;
    } else {
        createInfo.enabledLayerCount = 0;
    }

    VkResult result = vkCreateDevice(app->physicalDevice, &createInfo, NULL, &app->device);
    if (result != VK_SUCCESS) {
        perror("failed to create logical device!\n");
        exit(-1);
    }

    vkGetDeviceQueue(app->device, indices.graphicsFamily, 0, &app->graphicsQueue);
    vkGetDeviceQueue(app->device, indices.presentFamily, 0, &app->presentQueue);
}

void createSurface(HelloTriangleApplication *app) {
    if (glfwCreateWindowSurface(app->instance, app->window, NULL, &app->surface) != VK_SUCCESS) {
        perror("failed to create window surface!\n");
        exit(-1);
    }
}

bool checkDeviceExtensionSupport(VkPhysicalDevice device) {
    uint32_t extensionCount;
    vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount, NULL);
    VkExtensionProperties *availableExtensions = malloc(sizeof(VkExtensionProperties) * extensionCount);
    vkEnumerateDeviceExtensionProperties(device, NULL, &extensionCount, availableExtensions);
    for (size_t i = 0; i < sizeof(deviceExtensions) / sizeof(deviceExtensions[0]); i++) {
        bool has_extension = false;
        for (size_t j = 0; j < extensionCount; j++) {
            if (strcmp(deviceExtensions[i], availableExtensions[j].extensionName) == 0) {
                has_extension = true;
                break;
            }
        }
        if (!has_extension) {
            free(availableExtensions);
            return false;
        }
    }
    free(availableExtensions);
    return true;
}

SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, HelloTriangleApplication *app) {
    SwapChainSupportDetails details;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, app->surface, &details.capabilities);

    vkGetPhysicalDeviceSurfaceFormatsKHR(device, app->surface, &details.formatCount, NULL);
    if (details.formatCount != 0) {
        details.formats = malloc(sizeof(VkSurfaceFormatKHR) * details.formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, app->surface, &details.formatCount, details.formats);
    }

    vkGetPhysicalDeviceSurfacePresentModesKHR(device, app->surface, &details.presentModeCount, NULL);
    if (details.presentModeCount != 0) {
        details.presentModes = malloc(sizeof(VkPresentModeKHR) * details.presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, app->surface, &details.presentModeCount, details.presentModes);
    }

    return details;
}

VkSurfaceFormatKHR chooseSwapSurfaceFormat(const VkSurfaceFormatKHR *availableFormats, uint32_t formatCount) {
    for (uint32_t i = 0; i < formatCount; i++) {
        if (availableFormats[i].format == VK_FORMAT_B8G8R8A8_UNORM
            && availableFormats->colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            return availableFormats[i];
        }
    }

    return availableFormats[0];
}

VkPresentModeKHR chooseSwapPresentMode(const VkPresentModeKHR *availablePresentModes, uint32_t presentModeCount) {
    for (uint32_t i = 0; i < presentModeCount; i++) {
        if (availablePresentModes[i] == VK_PRESENT_MODE_FIFO_KHR) {
            return availablePresentModes[i];
        }
    }
    return VK_PRESENT_MODE_FIFO_KHR;
}

VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR *capabilities, HelloTriangleApplication *app) {
    if (capabilities->currentExtent.width != UINT32_MAX) {
        return capabilities->currentExtent;
    } else {
        int width, height;
        glfwGetFramebufferSize(app->window, &width, &height);

        VkExtent2D actualExtent = {
                (uint32_t) width,
                (uint32_t) height
        };
        actualExtent.width = max(capabilities->minImageExtent.width, min(capabilities->maxImageExtent.width, actualExtent.width));
        actualExtent.height = max(capabilities->minImageExtent.height, min(capabilities->maxImageExtent.height, actualExtent.height));

        return actualExtent;
    }
}

void createSwapChain(HelloTriangleApplication *app) {
    SwapChainSupportDetails swapChainSupport = querySwapChainSupport(app->physicalDevice, app);

    VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats, swapChainSupport.formatCount);
    VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes,
                                                         swapChainSupport.presentModeCount);
    VkExtent2D extent = chooseSwapExtent(&swapChainSupport.capabilities, app);
    if (swapChainSupport.presentModeCount != 0) {
        free(swapChainSupport.presentModes);
    }
    if (swapChainSupport.formatCount != 0) {
        free(swapChainSupport.formats);
    }

    uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createInfo = { 0 };
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = app->surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    QueueFamilyIndices indices = findQueueFamilies(app->physicalDevice, app);
    uint32_t queueFamilyIndices[] = {indices.graphicsFamily, indices.presentFamily};

    if (indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    } else {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }

    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = app->oldSwapchain;

    if (vkCreateSwapchainKHR(app->device, &createInfo, NULL, &app->swapChain) != VK_SUCCESS) {
        perror("failed to create swap chain!");
        exit(-1);
    }

    vkGetSwapchainImagesKHR(app->device, app->swapChain, &imageCount, NULL);
    app->swapChainImages = malloc(sizeof(VkImage) * imageCount);
    memset(app->swapChainImages, 0, sizeof(VkImage)*imageCount);
    vkGetSwapchainImagesKHR(app->device, app->swapChain, &imageCount, app->swapChainImages);

    app->swapChainImageFormat = surfaceFormat.format;
    app->swapChainExtent = extent;
    app->swapChainImageCount = imageCount;
}

void createImageViews(HelloTriangleApplication *app) {
    app->swapChainImageViews = malloc(sizeof(VkImageView) * app->swapChainImageCount);

    for (uint32_t i = 0; i < app->swapChainImageCount; i++) {
        VkImageViewCreateInfo createInfo = { 0 };
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = app->swapChainImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = app->swapChainImageFormat;
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
        createInfo.subresourceRange.levelCount = 1;
        createInfo.subresourceRange.baseArrayLayer = 0;
        createInfo.subresourceRange.layerCount = 1;

        if (vkCreateImageView(app->device, &createInfo, NULL, &app->swapChainImageViews[i]) != VK_SUCCESS) {
            perror("failed to create image views!\n");
            exit(-1);
        }
    }
}

void createGraphicsPipeline(HelloTriangleApplication *app) {
    size_t vertShaderCodeSize;
    char *vertShaderCode;
    if (readFile("shaders/vert.spv", &vertShaderCodeSize, &vertShaderCode)) exit(-1);
    size_t fragShaderCodeSize;
    char *fragShaderCode;
    if (readFile("shaders/frag.spv", &fragShaderCodeSize, &fragShaderCode)) exit(-1);

    VkShaderModule vertShaderModule = createShaderModule(vertShaderCode, vertShaderCodeSize, app);
    VkShaderModule fragShaderModule = createShaderModule(fragShaderCode, fragShaderCodeSize, app);

    VkPipelineShaderStageCreateInfo vertShaderStageInfo = { 0 };
    vertShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    vertShaderStageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
    vertShaderStageInfo.module = vertShaderModule;
    vertShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo fragShaderStageInfo = { 0 };
    fragShaderStageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    fragShaderStageInfo.stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    fragShaderStageInfo.module = fragShaderModule;
    fragShaderStageInfo.pName = "main";

    VkPipelineShaderStageCreateInfo shaderStages[] = {vertShaderStageInfo, fragShaderStageInfo};

    VkVertexInputBindingDescription bindingDescription = getBindingDescription();
    VkVertexInputAttributeDescription attributeDescriptions[2];
    getAttributeDescriptions(attributeDescriptions);

    VkPipelineVertexInputStateCreateInfo vertexInputInfo = { 0 };
    vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputInfo.vertexBindingDescriptionCount = 1;
    vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
    vertexInputInfo.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(attributeDescriptions[0]);
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions;

    VkPipelineInputAssemblyStateCreateInfo inputAssembly = { 0 };
    inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssembly.primitiveRestartEnable = VK_FALSE;


    VkPipelineViewportStateCreateInfo viewportState = { 0 };
    viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportState.viewportCount = 1;
    viewportState.pViewports = NULL;
    viewportState.scissorCount = 1;
    viewportState.pScissors = NULL;

    VkPipelineRasterizationStateCreateInfo rasterizer = { 0 };
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1.0f;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;

    VkPipelineMultisampleStateCreateInfo multisampling = { 0 };
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

    VkPipelineColorBlendAttachmentState colorBlendAttachment = { 0 };
    colorBlendAttachment.colorWriteMask =
            VK_COLOR_COMPONENT_R_BIT |
            VK_COLOR_COMPONENT_G_BIT |
            VK_COLOR_COMPONENT_B_BIT |
            VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = VK_FALSE;

    VkPipelineColorBlendStateCreateInfo colorBlending = { 0 };
    colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlending.logicOpEnable = VK_FALSE;
    colorBlending.logicOp = VK_LOGIC_OP_COPY;
    colorBlending.attachmentCount = 1;
    colorBlending.pAttachments = &colorBlendAttachment;
    colorBlending.blendConstants[0] = 0.0f;
    colorBlending.blendConstants[1] = 0.0f;
    colorBlending.blendConstants[2] = 0.0f;
    colorBlending.blendConstants[3] = 0.0f;

    VkPipelineLayoutCreateInfo pipelineLayoutInfo = { 0 };
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &app->descriptorSetLayout;

    if (vkCreatePipelineLayout(app->device, &pipelineLayoutInfo, NULL, &app->pipelineLayout) != VK_SUCCESS) {
        perror("failed to create pipeline layout!\n");
        exit(-1);
    }

    VkDynamicState dynamicStates[] = {
            VK_DYNAMIC_STATE_VIEWPORT,
            VK_DYNAMIC_STATE_SCISSOR,
    };

    VkPipelineDynamicStateCreateInfo dynamicState = { 0 };
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = sizeof(dynamicStates) / sizeof(dynamicStates[0]);
    dynamicState.pDynamicStates = dynamicStates;

    VkGraphicsPipelineCreateInfo pipelineInfo = { 0 };
    pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineInfo.stageCount = 2;
    pipelineInfo.pStages = shaderStages;
    pipelineInfo.pVertexInputState = &vertexInputInfo;
    pipelineInfo.pInputAssemblyState = &inputAssembly;
    pipelineInfo.pViewportState = &viewportState;
    pipelineInfo.pRasterizationState = &rasterizer;
    pipelineInfo.pMultisampleState = &multisampling;
    pipelineInfo.pDepthStencilState = NULL;
    pipelineInfo.pColorBlendState = &colorBlending;
    pipelineInfo.pDynamicState = &dynamicState;
    pipelineInfo.layout = app->pipelineLayout;
    pipelineInfo.renderPass = app->renderPass;
    pipelineInfo.subpass = 0;
    pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

    if (vkCreateGraphicsPipelines(app->device, VK_NULL_HANDLE, 1, &pipelineInfo, NULL, &app->graphicsPipeline) !=
        VK_SUCCESS) {
        perror("failed to create graphics pipeline\n");
        exit(-1);
    }

    vkDestroyShaderModule(app->device, fragShaderModule, NULL);
    vkDestroyShaderModule(app->device, vertShaderModule, NULL);
    free(vertShaderCode);
    free(fragShaderCode);
}

int readFile(const char *filename, size_t *size, char **buffer) {
    if (size == NULL || filename == NULL) {
        return -1;
    }

    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        perror("failed to open file\n");
        return -1;
    }
    fseek(file, 0, SEEK_END);
    *size = (size_t) ftell(file);
    fseek(file, 0, SEEK_SET);
    *buffer = malloc(*size);
    if (fread(*buffer, *size, 1, file) != 1) {
        perror("failed to read from file\n");
        free(*buffer);
        fclose(file);
        return -1;
    }
    fclose(file);
    return 0;
}

VkShaderModule createShaderModule(const char *code, size_t size, HelloTriangleApplication *app) {
    VkShaderModuleCreateInfo createInfo = { 0 };
    createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = size;
    createInfo.pCode = (uint32_t *) code;

    VkShaderModule shaderModule;
    if (vkCreateShaderModule(app->device, &createInfo, NULL, &shaderModule) != VK_SUCCESS) {
        perror("failed to creat shader module!\n");
        exit(-1);
    }

    return shaderModule;
}

void createRenderPass(HelloTriangleApplication *app) {
    VkAttachmentDescription colorAttachment = { 0 };
    colorAttachment.format = app->swapChainImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;


    VkAttachmentReference colorAttachmentRef = { 0 };
    colorAttachmentRef.attachment = 0;
    colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass = { 0 };
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    VkSubpassDependency dependency = { 0 };
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    VkRenderPassCreateInfo renderPassInfo = { 0 };
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = 1;
    renderPassInfo.pAttachments = &colorAttachment;
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;


    if (vkCreateRenderPass(app->device, &renderPassInfo, NULL, &app->renderPass) != VK_SUCCESS) {
        perror("failed to create render pass!\n");
        exit(-1);
    }

}

void createFramebuffers(HelloTriangleApplication *app) {
    app->swapChainFramebuffers = malloc(sizeof(VkFramebuffer) * app->swapChainImageCount);

    for (uint32_t i = 0; i < app->swapChainImageCount; i++) {
        VkImageView attachments[] = {
                app->swapChainImageViews[i]
        };

        VkFramebufferCreateInfo framebufferInfo = {0 };
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = app->renderPass;
        framebufferInfo.attachmentCount = 1;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = app->swapChainExtent.width;
        framebufferInfo.height = app->swapChainExtent.height;
        framebufferInfo.layers = 1;

        if (vkCreateFramebuffer(app->device, &framebufferInfo, NULL, &app->swapChainFramebuffers[i]) != VK_SUCCESS) {
            perror("failed to create framebuffer!\n");
            exit(-1);
        }
    }
}

void createCommandPool(HelloTriangleApplication *app) {
    QueueFamilyIndices queueFamilyIndices = findQueueFamilies(app->physicalDevice, app);

    VkCommandPoolCreateInfo poolInfo = { 0 };
    poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily;

    if (vkCreateCommandPool(app->device, &poolInfo, NULL, &app->commandPool) != VK_SUCCESS) {
        perror("failed to create command pool!\n");
        exit(-1);
    }
}

void createCommandBuffers(HelloTriangleApplication *app) {
    app->commandBuffers = malloc(sizeof(VkCommandBuffer) * app->swapChainImageCount);

    VkCommandBufferAllocateInfo allocInfo = { 0 };
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = app->commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = app->swapChainImageCount;

    if (vkAllocateCommandBuffers(app->device, &allocInfo, app->commandBuffers) != VK_SUCCESS) {
        perror("failed to allocate command buffers!\n");
        exit(-1);
    }

    for (uint32_t i = 0; i < app->swapChainImageCount; i++) {
        VkCommandBufferBeginInfo beginInfo = { 0 };
        beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        beginInfo.flags = 0;
        beginInfo.pInheritanceInfo = NULL;

        if (vkBeginCommandBuffer(app->commandBuffers[i], &beginInfo) != VK_SUCCESS) {
            perror("failed to begin recording command buffer!\n");
            exit(-1);
        }

        VkRenderPassBeginInfo renderPassInfo = { 0 };
        renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        renderPassInfo.renderPass = app->renderPass;
        renderPassInfo.framebuffer = app->swapChainFramebuffers[i];
        renderPassInfo.renderArea.offset = (VkOffset2D) {0, 0};
        renderPassInfo.renderArea.extent = app->swapChainExtent;

        VkClearValue clearColor = (VkClearValue){{{0.0f, 0.0f, 0.0f, 1.0f}}};
        renderPassInfo.clearValueCount = 1;
        renderPassInfo.pClearValues = &clearColor;

        vkCmdBeginRenderPass(app->commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        vkCmdBindPipeline(app->commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, app->graphicsPipeline);

        VkViewport viewport = { 0 };
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = (float) app->swapChainExtent.width;
        viewport.height = (float) app->swapChainExtent.height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        vkCmdSetViewport(app->commandBuffers[i], 0, 1, &viewport);

        VkRect2D scissor = { 0 };
        scissor.offset = (VkOffset2D) {0.0, 0.0};
        scissor.extent = app->swapChainExtent;

        vkCmdSetScissor(app->commandBuffers[i], 0, 1, &scissor);

        VkBuffer vertexBuffers[] = {app->vertexBuffer};
        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(app->commandBuffers[i], 0, 1, vertexBuffers, offsets);
        vkCmdBindIndexBuffer(app->commandBuffers[i], app->indexBuffer, 0, VK_INDEX_TYPE_UINT16);

        vkCmdBindDescriptorSets(app->commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, app->pipelineLayout, 0, 1, &app->descriptorSets[0], 0, NULL);

        vkCmdDrawIndexed(app->commandBuffers[i], sizeof(indices)/sizeof(indices[0]), 1, 0, 0, 0);
        vkCmdEndRenderPass(app->commandBuffers[i]);

        if (vkEndCommandBuffer(app->commandBuffers[i]) != VK_SUCCESS) {
            perror("failed to record command buffer!\n");
            exit(-1);
        }
    }
}

void drawFrame(HelloTriangleApplication *app) {

    vkWaitForFences(app->device, 1, &app->inFlightFences[app->currentFrame], VK_TRUE, UINT64_MAX);

    uint32_t imageIndex;
    VkResult result = vkAcquireNextImageKHR(app->device, app->swapChain, UINT64_MAX, app->imageAvailableSemaphore[app->currentFrame], VK_NULL_HANDLE, &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        recreateSwapChain(app);
        return;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        perror("failed to acquire swap chain image!\n");
        exit(-1);
    }

    updateUniformBuffer(imageIndex, app);

    if (app->imagesInFlight[imageIndex] != VK_NULL_HANDLE) {
        vkWaitForFences(app->device, 1, &app->imagesInFlight[imageIndex], VK_TRUE, UINT64_MAX);
    }
    app->imagesInFlight[imageIndex] = app->inFlightFences[app->currentFrame];

    VkSubmitInfo submitInfo = {0};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkSemaphore waitSemaphores[] = {app->imageAvailableSemaphore[app->currentFrame]};
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = waitSemaphores;
    submitInfo.pWaitDstStageMask = waitStages;

    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &app->commandBuffers[imageIndex];

    VkSemaphore signalSemaphores[] = {app->renderFinishedSemaphore[app->currentFrame]};
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = signalSemaphores;

    vkResetFences(app->device, 1, &app->inFlightFences[app->currentFrame]);

    if (vkQueueSubmit(app->graphicsQueue, 1, &submitInfo, app->inFlightFences[app->currentFrame]) != VK_SUCCESS) {
        perror("failed to submit draw command buffer!\n");
        exit(-1);
    }

    VkPresentInfoKHR presentInfo = {0};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = signalSemaphores;

    VkSwapchainKHR swapChains[] = {app->swapChain};
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = swapChains;

    presentInfo.pImageIndices = &imageIndex;

    result = vkQueuePresentKHR(app->presentQueue, &presentInfo);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || app->framebufferResized) {
        app->framebufferResized = false;
        recreateSwapChain(app);
    } else if (result != VK_SUCCESS) {
        perror("failed to present swap chain image!\n");
        exit(-1);
    }

    app->currentFrame = (app->currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void createSyncObjects(HelloTriangleApplication *app) {
    app->imageAvailableSemaphore = malloc(sizeof(VkSemaphore) * MAX_FRAMES_IN_FLIGHT);
    app->renderFinishedSemaphore = malloc(sizeof(VkSemaphore) * MAX_FRAMES_IN_FLIGHT);
    app->inFlightFences = malloc(sizeof(VkFence) * MAX_FRAMES_IN_FLIGHT);
    app->imagesInFlight = malloc(sizeof(VkFence) * app->swapChainImageCount);
    for (size_t i = 0; i < app->swapChainImageCount; i++) {
        app->imagesInFlight[i] = VK_NULL_HANDLE;
    }

    VkSemaphoreCreateInfo semaphoreInfo = { 0 };
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceInfo = { 0 };
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateSemaphore(app->device, &semaphoreInfo, NULL, &app->imageAvailableSemaphore[i]) != VK_SUCCESS ||
            vkCreateSemaphore(app->device, &semaphoreInfo, NULL, &app->renderFinishedSemaphore[i]) != VK_SUCCESS ||
            vkCreateFence(app->device, &fenceInfo, NULL, &app->inFlightFences[i]) != VK_SUCCESS) {
            perror("failed to create synchronization objects for a frame\n");
            exit(-1);
        }
    }
}

void recreateSwapChain(HelloTriangleApplication *app) {
    int width = 0;
    int height = 0;
    glfwGetFramebufferSize(app->window, &width, &height);
    while (width == 0 || height == 0) {
        glfwGetFramebufferSize(app->window, &width, &height);
        glfwWaitEvents();
    }

    cleanupSwapChain(app);


    app->oldSwapchain = app->swapChain;
    app->oldSwapChainImages = app->swapChainImages;
    app->oldImageFormat = app->swapChainImageFormat;
    app->oldImageCount = app->swapChainImageCount;
    createSwapChain(app);

    app->oldImageViews = app->swapChainImageViews;
    createImageViews(app);

    if (app->swapChainImageFormat != app->oldImageFormat) {
        app->oldRenderPass = app->renderPass;
        createRenderPass(app);
    } else {
        app->oldRenderPass = VK_NULL_HANDLE;
    }


    app->oldFramebuffers = app->swapChainFramebuffers;
    createFramebuffers(app);

    app->oldUniformBuffers = app->uniformBuffers;
    app->oldUniformBuffersMemory = app->uniformBuffersMemory;
    createUniformBuffers(app);

    app->oldDescriptorPool = app->descriptorPool;
    createDescriptorPool(app);
    app->oldDescriptorSets = app->descriptorSets;
    createDescriptorSets(app);

    app->oldCommandBuffers = app->commandBuffers;
    createCommandBuffers(app);

    app->oldInFlightFences = app->inFlightFences;
    app->oldImagesInFlight = app->imagesInFlight;
    app->inFlightFences = malloc(sizeof(VkFence) * MAX_FRAMES_IN_FLIGHT);
    app->imagesInFlight = malloc(sizeof(VkFence) * app->swapChainImageCount);
    for (size_t i = 0; i < app->swapChainImageCount; i++) {
        app->imagesInFlight[i] = VK_NULL_HANDLE;
    }
    VkFenceCreateInfo fenceInfo = { 0 };
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (vkCreateFence(app->device, &fenceInfo, NULL, &app->inFlightFences[i]) != VK_SUCCESS) {
            perror("failed to create synchronization objects for a frame\n");
            exit(-1);
        }
    }
}


void framebufferResizeCallback(GLFWwindow *window,__attribute__ ((unused)) int width,__attribute__ ((unused)) int height) {
    HelloTriangleApplication *app = (HelloTriangleApplication *) glfwGetWindowUserPointer(window);
    recreateSwapChain(app);
    drawFrame(app);
}

void cleanupSwapChain(HelloTriangleApplication *app) {
    if (app->oldInFlightFences != NULL) {
        vkWaitForFences(app->device, MAX_FRAMES_IN_FLIGHT, app->oldInFlightFences, VK_TRUE, UINT64_MAX);
        for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
            vkDestroyFence(app->device, app->oldInFlightFences[i], NULL);
        }
        free(app->oldInFlightFences);
    }


    if(app->oldUniformBuffers != NULL || app->oldUniformBuffersMemory != NULL) {
        for(size_t i = 0; i < app->oldImageCount; i++){
            vkDestroyBuffer(app->device, app->oldUniformBuffers[i], NULL);
            vkFreeMemory(app->device, app->oldUniformBuffersMemory[i], NULL);
        }
        free(app->oldUniformBuffers);
        free(app->oldUniformBuffersMemory);
    }

    if(app->oldDescriptorSets != NULL) {
        free(app->oldDescriptorSets);
    }

    if(app->oldDescriptorPool != VK_NULL_HANDLE) {
        vkDestroyDescriptorPool(app->device, app->oldDescriptorPool, NULL);
    }



    if(app->oldImagesInFlight != NULL) {
        free(app->oldImagesInFlight);
    }

    if (app->oldCommandBuffers != NULL) {
        vkFreeCommandBuffers(app->device, app->commandPool, app->oldImageCount, app->oldCommandBuffers);
        free(app->oldCommandBuffers);
    }


    for (size_t i = 0; i < app->oldImageCount; i++) {
        vkDestroyFramebuffer(app->device, app->oldFramebuffers[i], NULL);
        vkDestroyImageView(app->device, app->oldImageViews[i], NULL);
    }
    if (app->oldFramebuffers != NULL) {
        free(app->oldFramebuffers);
    }

    if (app->oldImageViews != NULL) {
        free(app->oldImageViews);
    }

    if (app->oldSwapChainImages != NULL) {
        free(app->oldSwapChainImages);
    }


    if (app->oldRenderPass != VK_NULL_HANDLE) {
        vkDestroyRenderPass(app->device, app->oldRenderPass, NULL);
    }

    if (app->oldSwapchain != VK_NULL_HANDLE) {
        vkDestroySwapchainKHR(app->device, app->oldSwapchain, NULL);
    }
}

void framebufferDamageCallback(GLFWwindow *window) {
    HelloTriangleApplication* app = (HelloTriangleApplication*)glfwGetWindowUserPointer(window);
    recreateSwapChain(app);
    drawFrame(app);
}

VkVertexInputBindingDescription getBindingDescription() {
    VkVertexInputBindingDescription bindingDescription = {0};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(Vertex);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;


    return bindingDescription;
}

void getAttributeDescriptions(VkVertexInputAttributeDescription description[2]) {
    if(description == NULL) return;
    description[0].binding = 0;
    description[0].location = 0;
    description[0].format = VK_FORMAT_R32G32_SFLOAT;
    description[0].offset = offsetof(Vertex, pos);

    description[1].binding = 0;
    description[1].location = 1;
    description[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    description[1].offset = offsetof(Vertex, color);
}

void createVertexBuffer(HelloTriangleApplication *app) {
    VkDeviceSize bufferSize = sizeof(vertices);

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &stagingBuffer, &stagingBufferMemory, app);

    void* data;
    vkMapMemory(app->device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, vertices, (size_t) bufferSize);
    vkUnmapMemory(app->device, stagingBufferMemory);

    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &app->vertexBuffer, &app->vertexBufferMemory, app);

    copyBuffer(stagingBuffer, app->vertexBuffer, bufferSize, app);

    vkDestroyBuffer(app->device, stagingBuffer, NULL);
    vkFreeMemory(app->device, stagingBufferMemory, NULL);
}

uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties, HelloTriangleApplication* app) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(app->physicalDevice, &memProperties);

    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties) {
            return i;
        }
    }

    perror("failed to find suitable memory type!\n");
    exit(-1);
}

void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer *buffer,
                  VkDeviceMemory *bufferMemory, HelloTriangleApplication* app) {
    VkBufferCreateInfo bufferInfo = {0};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if(vkCreateBuffer(app->device, &bufferInfo, NULL, buffer) != VK_SUCCESS) {
        perror("failed to create vertex buffer!\n");
        exit(-1);
    }

    VkMemoryRequirements memoryRequirements;
    vkGetBufferMemoryRequirements(app->device, *buffer, &memoryRequirements);

    VkMemoryAllocateInfo allocInfo = {0};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memoryRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memoryRequirements.memoryTypeBits, properties, app);

    if(vkAllocateMemory(app->device, &allocInfo, NULL, bufferMemory) != VK_SUCCESS) {
        perror("failed to allocate vertex buffer memory!\n");
        exit(-1);
    }

    vkBindBufferMemory(app->device, *buffer, *bufferMemory, 0);

}

void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size, HelloTriangleApplication* app) {
    VkCommandBufferAllocateInfo allocInfo = {0};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = app->commandPool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(app->device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo = {0};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    VkBufferCopy copyRegion = {0};
    copyRegion.size = size;
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);

    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo = {0};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vkQueueSubmit(app->graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(app->graphicsQueue);

    vkFreeCommandBuffers(app->device, app->commandPool, 1, &commandBuffer);
}

void createIndexBuffer(HelloTriangleApplication *app) {
    VkDeviceSize bufferSize = sizeof(indices);

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &stagingBuffer, &stagingBufferMemory, app);

    void* data;
    vkMapMemory(app->device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, indices, (size_t) bufferSize);
    vkUnmapMemory(app->device, stagingBufferMemory);

    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &app->indexBuffer, &app->indexBufferMemory, app);

    copyBuffer(stagingBuffer, app->indexBuffer, bufferSize, app);

    vkDestroyBuffer(app->device, stagingBuffer, NULL);
    vkFreeMemory(app->device, stagingBufferMemory, NULL);
}

void createDescriptorSetLayout(HelloTriangleApplication *app) {
    VkDescriptorSetLayoutBinding uboBindingLayout = {0};
    uboBindingLayout.binding = 0;
    uboBindingLayout.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboBindingLayout.descriptorCount = 1;
    uboBindingLayout.pImmutableSamplers = NULL;
    uboBindingLayout.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutCreateInfo layoutInfo = {0};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = 1;
    layoutInfo.pBindings = &uboBindingLayout;

    if(vkCreateDescriptorSetLayout(app->device, &layoutInfo, NULL, &app->descriptorSetLayout) != VK_SUCCESS) {
        perror("failed to create descriptor set layout!\n");
        exit(-1);
    }
}

void createUniformBuffers(HelloTriangleApplication *app) {
    VkDeviceSize  bufferSize = sizeof(UniformBufferObject);

    app->uniformBuffers = malloc(sizeof(VkBuffer)*app->swapChainImageCount);
    app->uniformBuffersMemory = malloc(sizeof(VkDeviceMemory)*app->swapChainImageCount);

    for (size_t i = 0; i < app->swapChainImageCount; i++) {
        createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, &app->uniformBuffers[i], &app->uniformBuffersMemory[i], app);
    }
}

void updateUniformBuffer(uint32_t currentImage, HelloTriangleApplication *app) {
    double currentTime = glfwGetTime();
    UniformBufferObject ubo = {0};
    vec3 eye = {2.0f, 2.0f, 2.0f};
    vec3 axis = {0.0f, 0.0f, 1.0f};
    vec3 center = {0.0f, 0.0f, 0.0f};
    vec3 up = {0.0f, 0.0f, 1.0f};
    glm_rotate_make(ubo.model, (float)sin(currentTime) * glm_rad(90.0f), axis);
    glm_lookat(eye, center,  up, ubo.view);
    glm_perspective(glm_rad(45.0f), app->swapChainExtent.width / (float) app->swapChainExtent.height, 0.1f, 10.0f, ubo.proj);
    //glm_mat4_mul(correction_matrix, ubo.proj, ubo.proj);
    ubo.proj[1][1] *= -1;

    void* data;
    vkMapMemory(app->device, app->uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
    memcpy(data, &ubo, sizeof(ubo));
    vkUnmapMemory(app->device, app->uniformBuffersMemory[currentImage]);

}

void createDescriptorPool(HelloTriangleApplication *app) {
    VkDescriptorPoolSize poolSize = {0};
    poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSize.descriptorCount = app->swapChainImageCount;

    VkDescriptorPoolCreateInfo poolInfo = {0};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = 1;
    poolInfo.pPoolSizes = &poolSize;
    poolInfo.maxSets = app->swapChainImageCount;

    if(vkCreateDescriptorPool(app->device, &poolInfo, NULL, &app->descriptorPool) != VK_SUCCESS) {
        perror("failed to create descriptor pool!\n");
        exit(-1);
    }
}

void createDescriptorSets(HelloTriangleApplication *app) {
    VkDescriptorSetLayout* layouts = malloc(sizeof(VkDescriptorSetLayout)*app->swapChainImageCount);
    for(size_t i = 0; i < app->swapChainImageCount; i++) {
        layouts[i] = app->descriptorSetLayout;
    }

    VkDescriptorSetAllocateInfo allocInfo = {0};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = app->descriptorPool;
    allocInfo.descriptorSetCount = app->swapChainImageCount;
    allocInfo.pSetLayouts = layouts;

    app->descriptorSets = malloc(sizeof(VkDescriptorSet)*app->swapChainImageCount);

    if(vkAllocateDescriptorSets(app->device, &allocInfo, app->descriptorSets) != VK_SUCCESS) {
        perror("failed to allocate descriptor sets!\n");
        exit(-1);
    }

    for(size_t i = 0; i < app->swapChainImageCount; i++) {
        VkDescriptorBufferInfo bufferInfo = {0};
        bufferInfo.buffer = app->uniformBuffers[i];
        bufferInfo.offset = 0;
        bufferInfo.range = sizeof(UniformBufferObject);

        VkWriteDescriptorSet descriptorWrite = {0};
        descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrite.dstSet = app->descriptorSets[i];
        descriptorWrite.dstBinding = 0;
        descriptorWrite.dstArrayElement = 0;
        descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrite.descriptorCount = 1;
        descriptorWrite.pBufferInfo = &bufferInfo;

        vkUpdateDescriptorSets(app->device, 1, &descriptorWrite, 0, NULL);

    }
    free(layouts);
}




